
var year=[];
        $(function () {
            var obj1 = {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Matches Per Year'
                },

                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: 'Number Of Matches'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Season',
                    data: []
                }]
            };
            jQuery.getJSON('../json/count.json', function (data) {
                obj1.xAxis.categories = Object.keys(data);
                obj1.series[0].data = Object.values(data);
                year=Object.keys(data);
                Highcharts.chart('count-container', obj1);
            });

        });
        $(function () {

            var obj = {

                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Number Of Matches Won By a Team Per Year'
                },

                xAxis: {
                    categories: []
                },

                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Number of wins'
                    }
                },

                tooltip: {
                    formatter: function () {
                        return '<b>' + this.x + '</b><br/>' +
                            this.series.name + ': ' + this.y + '<br/>' +
                            'Total: ' + this.point.stackTotal;
                    }
                },

                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },

                series: []
            };

            jQuery.getJSON('../json/wins.json', function (data) {
                obj.series=Object.keys(data).map((team)=>{
                        var nameValuePair={};
                        nameValuePair["name"]=team;
                        nameValuePair["data"]=Object.values(data[team]);
                        return nameValuePair;
                    });
                    console.log(obj);
                obj.xAxis.categories=year;
                Highcharts.chart('container', obj);
            });

        });

        $(function () {

            var obj3 = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Extra Runs per team in the year 2016'
                },
                xAxis: {
                    categories: [],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Extra runs'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [
                    {
                        name: 'extras',
                        data: []
                    }
                ]
            };
            jQuery.getJSON('../json/extras_runs.json', function (data) {
                obj3.xAxis.categories = Object.keys(data);
                obj3.series[0].data = Object.values(data);
                Highcharts.chart('extras-container', obj3);
            });
        });

        $(function () {
            var obj4 = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Economy of Bowlers'
                },
                xAxis: {
                    categories: [],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Economy'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: '2015',
                    data: []

                }]
            };
            jQuery.getJSON('./json/economy.json', function (data) {
                obj4.xAxis.categories = Object.keys(data).slice(0, 20);
                var tempData = Object.values(data);
                obj4.series[0].data = tempData.map(function (x) { return parseFloat(x) }).slice(0, 20);
                Highcharts.chart('economy-container', obj4)
            })
        })

        $(function () {
            // Build the chart
            var obj5 =            
            {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Players who got most number of player of match awards'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Players',
                    colorByPoint: true,
                    data: []
                }]
            };
            jQuery.getJSON("./json/pom.json",function(pom){
                var keys=Object.keys(pom);
                keys.forEach(function(key){
                    var temp={}
                    temp["name"]=key;
                    temp["y"]=pom[key];
                    obj5.series[0].data.push(temp);
                })
                Highcharts.chart('player-of-match-container', obj5);

            })
        })


