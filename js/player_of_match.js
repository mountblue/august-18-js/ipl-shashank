exports.countMatches=function(matchObj) {
    var matchCount=matchObj.reduce((acc,indObj) => {
        acc[indObj["player_of_match"]] = (acc[indObj["player_of_match"]] || 0) + 1;
        return acc;
    },{})
    return Object.keys(matchCount).reduce((result,player)=>{
        result.push([player,matchCount[player]]);
        return result;
    },[])
}