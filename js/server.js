const handler = require('serve-handler');
const http = require('http');
var fs = require("fs");
var csv = require("csvtojson")

var matchCount = require("./match_count.js");
var stackedBar = require("./stacked_bar.js");
var extras = require("./extras_runs.js");
var economy = require("./economy.js");
var playerOfMatch = require('./player_of_match.js');

var file = "./json/matches.json";

const server = http.createServer((request, response) => {
  // You pass two more arguments for config and middleware
  // More details here: https://github.com/zeit/serve-handler#options
  return handler(request, response);
})
server.listen(3000, () => {
  console.log('Running at http://localhost:3000');
});




if (!(fs.existsSync("./json/matches.json"))) {
  var csvFilePath = "./csv/matches.csv";
  csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
      var json = JSON.stringify(jsonObj);
      fs.writeFileSync('./json/matches.json', json, (error) => {
        if (error) console.log(error)
      });
    })
} else {
  var matchObj = JSON.parse(fs.readFileSync(file, 'utf8'));
  var countRes = matchCount.countMatches(matchObj);
  // console.log(countRes);
  fs.writeFile("./json/count.json", JSON.stringify(countRes), (error) => {
    if (error) console.log(error)
  })
}

var winners = stackedBar.getWinners(matchObj);
var json = JSON.stringify(stackedBar.convertObj(winners));
fs.writeFile('./json/wins.json', json, (error) => {
  if (error) console.log(error)
});


var deliveriesObj = JSON.parse(fs.readFileSync('./json/deliveries.json', 'utf8'))
var teamId = extras.getId(matchObj, 2016);
var extraRuns = extras.getExtras(teamId, deliveriesObj);
// console.log(extraRuns);
var json = JSON.stringify(extraRuns);
fs.writeFile("./json/extras_runs.json", json, (error) => {
  if (error) console.log(error)
});


var teamSet = extras.getId(matchObj, 2015);
var economy = economy.economy(teamSet, deliveriesObj);
var res = economy.reduce((acc,key)=>{
  acc[key[0]] = key[1];
  return acc;
},{});
var json = JSON.stringify(res);
fs.writeFile("./json/economy.json", json, (error) => {
  if (error) console.log(error)
});


var playerOfMatchCount = playerOfMatch.countMatches(matchObj);
playerOfMatchCount.sort(function (a, b) {
  return b[1] - a[1];
});
var topBatsman = playerOfMatchCount.slice(0, 10);
var batsman=topBatsman.reduce((acc,key)=>{
  acc[key[0]] = key[1];
  return acc;
},{});
var json = JSON.stringify(batsman);
fs.writeFile("./json/pom.json", json, (error) => {
  if (error) console.log(error)
});