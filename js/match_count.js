exports.countMatches=function(matchObj) {
    return matchObj.reduce((acc,cur)=>{
        acc[cur["season"]] = (acc[cur["season"]] || 0) + 1;
        return acc;},{})
}