
exports.getWinners = function (matchObj) {
  return matchObj.reduce((acc, cur) => {
    if (cur["winner"] in acc)
      acc[cur["winner"]].push(cur["season"]);
    else{
      acc[cur["winner"]] = new Array();
      acc[cur["winner"]].push(cur["season"]);
    }
    return acc;
  }, {})
}

exports.convertObj = function (matchCount) {
  let years = Object.values(matchCount).join(',');
  let Repeatedyears = years.split(',');
  let uniqueYear = Array.from(new Set(Repeatedyears));
  let resultObj = Object.keys(matchCount).reduce((acc, cur) => {
    acc[cur] = uniqueYear.reduce(function(count, year) {
      if(matchCount[cur].includes(year)){
        count[year] =getCount(year,matchCount[cur]);
      }
      else{
        count[year] = 0;
      }
      return count;
    },{})
    return acc;
  },{})

  function getCount(year,arr){
    return arr.reduce((acc,cur)=>{
      if(cur==year)
        acc=(acc||0)+1;
      return acc;
    },0)
  }
  return resultObj;
}