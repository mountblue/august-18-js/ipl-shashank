var assert = require('chai').assert;
var fs = require("fs")

var file = "./test/matches.json";

var calc = require('../js/match_count.js');
var calc1 = require('../js/stacked_bar.js');
var extras = require('../js/extras_runs.js');
var economy = require("../js/economy.js");
var pom = require('../js/player_of_match.js');

var expectedRes = {
	"Royal Challengers Bangalore": ["2017","2015"],
	"Sunrisers Hyderabad": ["2017","2016"]
}


describe('IPL Dataset Analysis Test', function () {
	var obj = JSON.parse(fs.readFileSync(file, 'utf8'));

	it('returns length of output', function () {
		var expectedRes = { '2015': 1, '2016': 1, '2017': 2 };
		assert.deepEqual(calc.countMatches(obj), expectedRes);
	});

	it('returns winners list', function () {
		var expectedRes = {
			"Royal Challengers Bangalore": ["2017","2015"],
			"Sunrisers Hyderabad": ["2017","2016"]
		}
		assert.deepEqual(calc1.getWinners(obj), expectedRes);
	});

	it('returns winners list', function () {
		var outputRes = {
			"Royal Challengers Bangalore": {"2015": 1,"2016": 0,"2017": 1},
			"Sunrisers Hyderabad": {"2015": 0,"2016": 1,"2017": 1}
		}
		assert.deepEqual(calc1.convertObj(expectedRes), outputRes);
	});



	it("Returns extra runs per team", function () {
		var matchId = [1, 2, 3, 4, 5];
		var input = [
			{
				match_id: '1',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '0'
			},
			{
				match_id: '2',
				batting_team: 'Chennai Super Kings',
				bowling_team: 'Mumbai Indians',
				extra_runs: '1'
			},
			{
				match_id: '3',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '5'
			},
			{
				match_id: '8',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '0'
			},
			{
				match_id: '3',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Royal Challengers Bangalore',
				extra_runs: '0'
			},
			{
				match_id: '3',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '2'
			},
			{
				match_id: '3',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '0'
			},
			{
				match_id: '8',
				batting_team: 'Mumbai Indians',
				bowling_team: 'Chennai Super Kings',
				extra_runs: '1'
			},
		];
		var extraRes = {
			"Chennai Super Kings": 7,
			"Mumbai Indians": 1
		};

		assert.deepEqual(extras.getExtras(matchId, input), extraRes);
	})

	it('returns player economy', function () {
		var matchId = [1, 2, 3, 4, 5];
		var input = [
			{
				match_id: '1',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'A Choudhary',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '4',
			},
			{
				match_id: '2',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'A Choudhary',
				is_super_over: '0',
				wide_runs: '0',
				batsman_runs: '4',
				noball_runs: '0',
				extra_runs: '0',
				total_runs: '4',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '3',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'A Choudhary',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '4',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '8',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'A Choudhary',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '4',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '3',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'MC Henriques',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '4',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '3', batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'MC Henriques',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '4',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '3',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'MC Henriques',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '0',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
			{
				match_id: '8',
				batting_team: 'Sunrisers Hyderabad',
				bowling_team: 'Royal Challengers Bangalore',
				over: '13',
				ball: '3',
				batsman: 'Yuvraj Singh',
				non_striker: 'MC Henriques',
				bowler: 'MC Henriques',
				is_super_over: '0',
				wide_runs: '0',
				bye_runs: '0',
				legbye_runs: '0',
				noball_runs: '0',
				penalty_runs: '0',
				batsman_runs: '4',
				extra_runs: '0',
				total_runs: '0',
				player_dismissed: '',
				dismissal_kind: '',
				fielder: ''
			},
		];

		var expectedRes = [['MC Henriques', '16.0000'],['A Choudhary', '24.0000']]

		assert.deepEqual(economy.economy(matchId,input), expectedRes);
	});

	it('returns player of match count', function () {
		var outputRes = [
			["abc", 1],
			["def", 1],
			["Yuvraj Singh",2]
		]

		assert.deepEqual(pom.countMatches(obj), outputRes);
	});

});