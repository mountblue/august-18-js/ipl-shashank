This project plots following graphs for IPL data from 2008-2017,
1. Matches played per year.
2. Number of wins per team per year.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.
5. Players who got maximum player of match awards.

Requirements for the code to execute are,
1. Active internet connection to access Highcharts library.
2. Local server. (Recommended server is serve).
3. Lts version of node.
